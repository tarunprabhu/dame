/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil ; -*- */

/*
 *  (C) 2001 by Argonne National Laboratory.
 *      See COPYRIGHT in top-level directory.
 */

#include <mpiimpl.h>
#include <mpidu_dataloop.h>
#include <stdlib.h>

/*@
  MPIDU_Type_commit
 
Input Parameters:
. datatype_p - pointer to MPI datatype

Output Parameters:

  Return Value:
  0 on success, -1 on failure.
@*/

int MPIDU_Type_commit(MPI_Datatype *datatype_p)
{
    int           mpi_errno=MPI_SUCCESS;
    MPIDU_Datatype *datatype_ptr;

    MPIR_Assert(HANDLE_GET_KIND(*datatype_p) != HANDLE_KIND_BUILTIN);

    MPIDU_Datatype_get_ptr(*datatype_p, datatype_ptr);

    if (datatype_ptr->is_committed == 0) {
       datatype_ptr->is_committed = 1;

#ifdef WITH_DAME
       /* Although we pass dataloop_size by pointer, it will not get set in 
        * the function. It was relatively easy to calculate the size when 
        * there were no significant optimizations of the datatype taking 
        * place when constructing the dataloop. But now that we aggressively
        * optimize the representation, calculating the size becomes difficult. 
        * So we run a second pass to calculate the actual size after all the 
        * optimizations have been performed. I'll continue to pass the 
        * parameter in case we later decide that we would rather not do 
        * another pass over the datatype and calculate the size "on the fly" */
       MPIDU_Dame_create(*datatype_p, 
                        &datatype_ptr->dataloop,
                        &datatype_ptr->dataloop_size,
                        &datatype_ptr->dataloop_depth);

       /* We need the dataloop size to compact it for the RMA code. */
       MPIDU_Dame_calculate_size(datatype_ptr->dataloop,
                                &datatype_ptr->dataloop_size,
                                0);

       /* After the call to Dataloop_create, the structs will be represented
        * without the additional space above the subtypes which DL_pack and
        * DL_unpack expect. While it's in this form, it's easier (and more 
        * space-efficient) to compact it. The call to Dataloop_update() will 
        * "fix" the dataloop to the form that DL_pack and DL_unpack expect. 
        * (for details see the comments in Dataloop_update()) */
       MPIDU_Dame_serialize(datatype_ptr->dataloop,
                           datatype_ptr->dataloop_size,
                           &datatype_ptr->compact_dataloop);

        /* This will adjust the inner types. See comments above and in the 
         * definition of Dataloop_update. This representation will be used for 
         * local operations and pt2pt communication. The compact representation
         * will be used in RMA operations and converted to this representation 
         * on the target */
       MPIDU_Dame_update(datatype_ptr->dataloop, 0);
#if 0
       PREPEND_PREFIX(Dame_print)(datatype_ptr->dataloop);
       PREPEND_PREFIX(Dame_print_compact)(datatype_ptr->compact_dataloop);
#endif
#else
#ifdef MPID_NEEDS_DLOOP_ALL_BYTES
        /* If MPID implementation needs use to reduce everything to
           a byte stream, do that. */
	MPIDU_Dataloop_create(*datatype_p,
			     &datatype_ptr->dataloop,
			     &datatype_ptr->dataloop_size,
			     &datatype_ptr->dataloop_depth,
			     MPIDU_DATALOOP_ALL_BYTES);
#else
	MPIDU_Dataloop_create(*datatype_p,
			     &datatype_ptr->dataloop,
			     &datatype_ptr->dataloop_size,
			     &datatype_ptr->dataloop_depth,
			     MPIDU_DATALOOP_HOMOGENEOUS);
#endif

	/* create heterogeneous dataloop */
	MPIDU_Dataloop_create(*datatype_p,
			     &datatype_ptr->hetero_dloop,
			     &datatype_ptr->hetero_dloop_size,
			     &datatype_ptr->hetero_dloop_depth,
			     MPIDU_DATALOOP_HETEROGENEOUS);

	MPL_DBG_MSG_D(MPIR_DBG_DATATYPE,TERSE,"# contig blocks = %d\n",
                       (int) datatype_ptr->max_contig_blocks);

#if 0
        MPIDI_Dataloop_dot_printf(datatype_ptr->dataloop, 0, 1);
#endif
#endif

#ifdef MPID_Type_commit_hook
       MPID_Type_commit_hook(datatype_ptr);
#endif /* MPID_Type_commit_hook */

    }
    return mpi_errno;
}


/* -*- Mode: C; c-basic-offset:4 ; indent-tabs-mode:nil ; -*- */

/*
 *  (C) 2001 by Argonne National Laboratory.
 *      See COPYRIGHT in top-level directory.
 */

/* This file is here only to keep includes in one place inside the
 * dame subdirectory. This file will be different for each use
 * of the dame code, but others should be identical.
 */
#ifndef DAME_H
#define DAME_H

/* This is specific to MPICH */
#include "mpiimpl.h"
#include "mpir_dame.h"

#endif



